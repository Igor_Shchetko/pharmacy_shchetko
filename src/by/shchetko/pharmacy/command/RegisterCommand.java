package by.shchetko.pharmacy.command;

import by.shchetko.pharmacy.command.constant.ConstantValue;
import by.shchetko.pharmacy.resource.ConfigurationManager;
import by.shchetko.pharmacy.resource.MessageManager;

import javax.servlet.http.HttpServletRequest;

public class RegisterCommand implements ActionCommand {
    @Override
    public String execute(HttpServletRequest request) {
        String page= null;
        boolean noErrors = true;
        // извлечение из запроса параметров регистрации
        String name = request.getParameter(ConstantValue.PARAM_NAME_NAME);
        String email = request.getParameter(ConstantValue.PARAM_NAME_EMAIL);
        String password = request.getParameter(ConstantValue.PARAM_NAME_PASSWORD);
        // проверка параметров регистрации
        if (name == null || name.equals("")) {
            request.setAttribute(ConstantValue.ATTRIBUTE_NAME_EMPTY_NAME_FIELD, MessageManager.getProperty(ConstantValue.MESSAGE_PROPERTY_NAME_REGISTER_ERROR));
            noErrors = false;
        }
        if (email == null || email.equals("")) {
            request.setAttribute(ConstantValue.ATTRIBUTE_NAME_EMPTY_EMAIL_FIELD, MessageManager.getProperty(ConstantValue.MESSAGE_PROPERTY_NAME_REGISTER_ERROR));
            noErrors = false;
        }
        if (password == null || password.equals("")) {
            request.setAttribute(ConstantValue.ATTRIBUTE_NAME_EMPTY_PASSWORD_FIELD, MessageManager.getProperty(ConstantValue.MESSAGE_PROPERTY_NAME_REGISTER_ERROR));
            noErrors = false;
        }
        if (noErrors) {
            request.setAttribute(ConstantValue.PARAM_NAME_NAME, name);
            request.setAttribute(ConstantValue.PARAM_NAME_EMAIL, email);
            request.setAttribute(ConstantValue.PARAM_NAME_PASSWORD, password);
            page = ConfigurationManager.getProperty(ConstantValue.PATH_PROPERTY_NAME_REGISTER_SUCCESS_PAGE);
        } else {
            page = ConfigurationManager.getProperty(ConstantValue.PATH_PROPERTY_NAME_REGISTRATION_PAGE);
        }
        return page;
    }
}