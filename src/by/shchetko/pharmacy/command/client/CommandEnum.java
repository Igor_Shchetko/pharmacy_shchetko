package by.shchetko.pharmacy.command.client;

import by.shchetko.pharmacy.command.ActionCommand;
import by.shchetko.pharmacy.command.LoginCommand;
import by.shchetko.pharmacy.command.LogoutCommand;
import by.shchetko.pharmacy.command.RegisterCommand;

public enum CommandEnum {
    LOGIN {
        {
            this.command = new LoginCommand();
        }
    },
    LOGOUT {
        {
            this.command = new LogoutCommand();
        }
    },
    REGISTER {
        {
            this.command = new RegisterCommand();
        }
    };
    ActionCommand command;
    public ActionCommand getCurrentCommand() {
        return command;
        }
}