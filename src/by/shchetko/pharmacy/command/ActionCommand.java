package by.shchetko.pharmacy.command;

import javax.servlet.http.HttpServletRequest;
public interface ActionCommand {
        String execute(HttpServletRequest request);
        }
