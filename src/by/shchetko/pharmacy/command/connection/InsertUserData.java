package by.shchetko.pharmacy.command.connection;

import by.shchetko.pharmacy.dao.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class InsertUserData {
    private final static String SQL_INSERT = "INSERT INTO user(name, email, password) VALUES(?,?,?,?)";
    private Connection connect;
    public InsertUserData() throws SQLException {
        connect = ConnectorDB.getConnection();
    }
    public PreparedStatement getPreparedStatement(){
        PreparedStatement ps = null;
        try{
            ps = connect.prepareStatement(SQL_INSERT);
        } catch(SQLException e) {
            e.printStackTrace();
        }
        return ps;
    }
    public boolean insertUserData(PreparedStatement ps, User user) {
        boolean flag = false;
        try{
            ps.setString(1, user.getName());
            ps.setString(2, user.getEmail());
            ps.setString(3, user.getPassword());
            ps.executeUpdate();
            flag = true;
        } catch(SQLException e) {
            e.printStackTrace();
        }
        return flag;
    }
    public void closeStatement(PreparedStatement ps) {
        if(ps != null) {
            try{
                ps.close();
            } catch(SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
