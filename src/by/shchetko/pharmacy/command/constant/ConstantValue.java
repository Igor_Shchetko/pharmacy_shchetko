package by.shchetko.pharmacy.command.constant;

public class ConstantValue {
    public static final String PARAM_NAME_NAME = "name";
    public static final String PARAM_NAME_EMAIL = "email";
    public static final String PARAM_NAME_PASSWORD = "password";
    public static final String ATTRIBUTE_NAME_EMPTY_NAME_FIELD = "errorNameRegisterMessage";
    public static final String ATTRIBUTE_NAME_EMPTY_EMAIL_FIELD = "errorEmailRegisterMessage";
    public static final String ATTRIBUTE_NAME_EMPTY_PASSWORD_FIELD = "errorPasswordRegisterMessage";
    public static final String PATH_PROPERTY_NAME_REGISTER_SUCCESS_PAGE = "path.page.register_success";
    public static final String PATH_PROPERTY_NAME_REGISTRATION_PAGE = "path.page.registration";
    public static final String MESSAGE_PROPERTY_NAME_REGISTER_ERROR = "message.register_error";
}
