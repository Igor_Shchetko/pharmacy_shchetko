package by.shchetko.pharmacy.resource;

import java.util.ResourceBundle;

public class MessageManager {
private final static ResourceBundle resourceBundle = ResourceBundle.getBundle("by.shchetko.pharmacy.resource.messages");
// класс извлекает информацию из файла messages.properties
private MessageManager() { }
public static String getProperty(String key) {
        return resourceBundle.getString(key);
        }
}
