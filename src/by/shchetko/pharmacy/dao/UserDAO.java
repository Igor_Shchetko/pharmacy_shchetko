package by.shchetko.pharmacy.dao;

import by.shchetko.pharmacy.command.constant.ConstantValue;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class UserDAO extends AbstractDAO <User> {
    public static final String SQL_SELECT_ALL_ABONENTS = "SELECT * FROM phonebook";
    public static final String SQL_SELECT_ABONENT_BY_LASTNAME = "SELECT idphonebook,phone FROM phonebook WHERE lastname=?";
    public UserDAO(Connection connection) {
        super(connection);
    }
    @Override
    public List<User> findAll() {
        List<User> users = new ArrayList<>();
        Connection cn = null;
        Statement st = null;
        try{
            cn = ConnectionPool.getConnection();
            st = cn.createStatement();
            ResultSet resultSet = st.executeQuery(SQL_SELECT_ALL_ABONENTS);
            while(resultSet.next()) {
                User user = new User();
                user.setName(resultSet.getString(ConstantValue.PARAM_NAME_NAME));
                user.setEmail(resultSet.getString(ConstantValue.PARAM_NAME_EMAIL));
                user.setPassword(resultSet.getString(ConstantValue.PARAM_NAME_PASSWORD));
                users.add(user);
            }
        } catch(SQLException e) {
            //LOG!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            System.err.println("SQL exception (request or table failed): " + e);
        } finally{
            close(st);
// код возвращения экземпляра Connection в пул
        }
        return users;
        }
        @Override
        public User findEntityById(Integer id) {
            throw new UnsupportedOperationException();
        }
@Override
public boolean delete(Integer id) {
            throw new UnsupportedOperationException();
        }
        @Override
        public User create(User entity) {
            throw new UnsupportedOperationException();
        }
        @Override
        public User update(User entity) {
            throw new UnsupportedOperationException();
        }
// собственный метод DAO
        public User findUserByLastName(String name) {
            User user = new User();
            Connection cn = null;
            PreparedStatement st = null;
            try{
                cn = ConnectionPool.getConnection();
                st =
                        cn.prepareStatement(SQL_SELECT_ABONENT_BY_LASTNAME);
                st.setString(1, name);
                ResultSet resultSet =st.executeQuery();
                resultSet.next();
                user.setId(resultSet.getInt("idphonebook"));
                user.setPhone(resultSet.getInt("phone"));
                user.setLastName(name);
            } catch(SQLException e) {
                System.err.println("SQL exception (request or table failed): " + e);
            } finally{
                close(st);
// код возвращения экземпляра Connection в пул
            }
            return user;
        }
@Override
public boolean delete(User entity) {
            throw new UnsupportedOperationException();
        }
    }
}
