//package by.shchetko.pharmacy.logic;
//
//import by.shchetko.pharmacy.resource.ConfigurationManager;
//import by.shchetko.pharmacy.resource.MessageManager;
//
//import javax.servlet.RequestDispatcher;
//import javax.servlet.ServletException;
//import java.io.PrintWriter;
//import java.sql.Connection;
//import java.sql.PreparedStatement;
//import java.sql.SQLException;
//
//public class RegisterLogic {
//    //??????????????????????????????????
//    public static boolean checkRegistration(String name, String email, String password) {
//        boolean noErrors = true;
//        if (name == null || name.equals("")) {
//            request.setAttribute("errorNameRegisterMessage", MessageManager.getProperty("message.register_error"));
//            noErrors = false;
//        }
//        if (email == null || email.equals("")) {
//            request.setAttribute("errorEmailRegisterMessage", MessageManager.getProperty("message.register_error"));
//            noErrors = false;
//        }
//        if (password == null || password.equals("")) {
//            request.setAttribute("errorPasswordRegisterMessage", MessageManager.getProperty("message.register_error"));
//            noErrors = false;
//        }
//        if (errorMsg != null) {
//            RequestDispatcher rd = getServletContext().getRequestDispatcher("/register.html");
//            PrintWriter out= response.getWriter();
//            out.println("<font color=red>"+errorMsg+"</font>");
//            rd.include(request, response);
//        } else {
//            Connection con = (Connection) getServletContext().getAttribute("DBConnection");
//            PreparedStatement ps = null;
//            try {
//                ps = con.prepareStatement("insert into Users(name,email,country, password) values (?,?,?,?)");
//                ps.setString(1, name);
//                ps.setString(2, email);
//                ps.setString(3, country);
//                ps.setString(4, password);
//
//                ps.execute();
//
//                logger.info("User registered with email="+email);
//
//                //forward to login page to login
//                RequestDispatcher rd = getServletContext().getRequestDispatcher("/login.html");
//                PrintWriter out= response.getWriter();
//                out.println("<font color=green>Registration successful, please login below.</font>");
//                rd.include(request, response);
//            } catch (SQLException e) {
//                e.printStackTrace();
//                logger.error("Database connection problem");
//                throw new ServletException("DB Connection problem.");
//            } finally {
//                try {
//                    ps.close();
//                } catch (SQLException e) {
//                    logger.error("SQLException in closing PreparedStatement");
//                }
//            }
//        }
//    }
//}
