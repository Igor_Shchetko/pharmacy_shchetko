<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
    <title>Registration Page</title>
</head>
<body>
<h3>Provide all the fields for registration.</h3>
<form name="registrationForm" method="POST" action="controller">
    <input type="hidden" name="command" value="register"/>
    <strong>Name</strong>:<input type="text" name="name"><br>
    <br/>
    ${errorNameRegisterMessage}
    <br/>
    <strong>Email ID</strong>:<input type="text" name="email"><br>
    <br/>
    ${errorEmailRegisterMessage}
    <br/>
    <strong>Password</strong>:<input type="password" name="password"><br>
    <br/>
    ${errorPasswordRegisterMessage}
    <br/>
    <input type="submit" value="Register">
</form>
<br>
If you are registered user, please <a href="controller?command=login">login</a>.
</body>
</html>