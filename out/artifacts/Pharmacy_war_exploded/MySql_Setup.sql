-- login with root to create user, DB and table and provide grants
 
create user 'igor'@'localhost' identified by 'igor';

grant all on *.* to 'igor'@'localhost' identified by 'igor';

create database PharmacyDB;

use PharmacyDB;

CREATE TABLE `Users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL DEFAULT '',
  `email` varchar(20) NOT NULL DEFAULT '',
  `password` varchar(20) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
